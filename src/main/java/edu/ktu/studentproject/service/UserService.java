package edu.ktu.studentproject.service;


import java.util.List;

import edu.ktu.studentproject.dto.User;


public interface UserService {

    User save(User user);
    List<User> findAll();
    User findOne(long id);
    void delete(long id);
}