package edu.ktu.studentproject.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import edu.ktu.studentproject.dto.Student;
import edu.ktu.studentproject.repository.StudentRepository;

@Service
public class StudentService {
	
@Autowired
private StudentRepository studentRepository;



public List<Student> getStudents(){
	
	List<Student> students = new ArrayList<>();
	studentRepository.findAll()
	.forEach(students::add);
	return students;
}

public Optional<Student> getStudent (String id) {
	
     return studentRepository.findById(id);
}

public void addStudent(Student student) {
	
	((CrudRepository<Student, String>) studentRepository).save(student);
}



public void updateStudent(String id, Student student) {
	
	((CrudRepository<Student, String>) studentRepository).save(student);
}

public void deleteStudent(String id) {
	
	((CrudRepository<Student, String>) studentRepository).deleteById(id);
}



}
