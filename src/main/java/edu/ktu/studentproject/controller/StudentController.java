package edu.ktu.studentproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import edu.ktu.studentproject.dto.Student;
import edu.ktu.studentproject.service.StudentService;

@CrossOrigin
@RestController
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/student")
	public List<Student> getStudents() {
		return studentService.getStudents();
	}
     
	@RequestMapping("/student/{id}")
	public Optional<Student> getTopic (@PathVariable String id) {
		return studentService.getStudent(id);
	}
	
	@RequestMapping(method = RequestMethod.POST , value="/student")
	public void addStudent (@RequestBody Student student) {
		studentService.addStudent(student);
	}
	@RequestMapping(method = RequestMethod.PUT , value="/student/{id}")
	public void updateStudent (@RequestBody Student student,@PathVariable String id) {
		studentService.updateStudent(id,student);
	}
	@RequestMapping(method = RequestMethod.DELETE , value="/student/{id}")
	public void deleteStudent (@PathVariable String id) {
		studentService.deleteStudent(id);
	}
	
}