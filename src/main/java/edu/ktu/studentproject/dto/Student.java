package edu.ktu.studentproject.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
@Entity
@Table(name = "create_project")
public class Student {
	
    @Id
    private String id;
    @Column(name="project_description", nullable = true)
    @Type(type="text")
    private String project_description;
    @Column(name="institution", nullable = true)
    @Type(type="text")
	
    
	private String institution;
	
	
	public Student() {
		
	}
	
	public Student(String id, String project_description, String institution) {
		super();
		this.id = id;
		this.project_description = project_description;
		this.institution = institution;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getProject_description() {
		return project_description;
	}

	public void setProject_description(String project_description) {
		this.project_description = project_description;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}
	
	
	

}