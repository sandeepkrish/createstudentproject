package edu.ktu.studentproject.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import edu.ktu.studentproject.dto.Student;

@RepositoryRestResource
public interface StudentRepository extends CrudRepository<Student ,String> {

	
	

}
